document.querySelector('.button').addEventListener('click',(event)=>{
    event.preventDefault();
    document.body.classList.toggle('light');
    if (document.body.matches('.light')){
        localStorage.setItem('theme','light');
    }else{
        localStorage.setItem('theme','');
    }
})
let active = localStorage.getItem('theme');
if (active === 'light'){
    document.body.classList.add('light');
}